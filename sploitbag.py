#!/usr/bin/env python3

import sys, os, argparse
import coloredlogs, logging

from sploitbag.main import SploitBagInterpreter

logging.getLogger('paramiko').setLevel(logging.WARNING)
coloredlogs.install(level=logging.DEBUG,
                    fmt='%(asctime)s,%(msecs)03d %(name)s[%(filename)s] %(levelname)s %(message)s')

parser = argparse.ArgumentParser(description='h4x1ng n3tw0rks f0r fun')
parser.add_argument('CIDR', metavar='CIDR', help='CIDR or "autoheck"')
parser.add_argument('-a', default='mapping', help='"mapping" for mapping, and "exploiting" for exploiting xD')
parser.add_argument('-c', help='Continuous fun')


if __name__ == "__main__":
    if os.geteuid() != 0:
        sys.exit('This script must be run as root!')
    elif sys.version_info.major < 3:
        sys.exit('SploutBag supports only Python3. Rerun application in Python3 environment.')
    else:
        SploitBagInterpreter(parser.parse_args()).start()
