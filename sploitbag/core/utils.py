import ifaddr
import logging


def detect_cidrs():
    cidrs = []
    adapters = ifaddr.get_adapters()
    logging.info('Using "autoheck" we will try to detect CIDRs in your adapters and choose the most proper ones. '
                 'You can use manual mode if you don’t agree with this automatic decision or if it doesn’t work fine :)')
    for adapter in adapters:
        for ip in adapter.ips:
            cidr = '{}/{}'.format(ip.ip, ip.network_prefix)
            if 16 <= ip.network_prefix <= 32:
                cidrs.append(cidr)
                logging.info('Will heck CIDR {} from adapter {}'.format(cidr, adapter.nice_name))
            else:
                logging.debug('Adapter {} contains CIDR {}. We will skip it :P'.format(adapter.nice_name, cidr))
    return cidrs
