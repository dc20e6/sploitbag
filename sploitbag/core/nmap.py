import logging

import nmap


class NetworkMapper():
    def __init__(self):
        self.devices_count = 0
        self.devices_list = []

    def map_network(self, cidr):
        logging.info('Starting the mapping of the network with CIDR: {} (size: {})'.format(cidr, cidr.size))
        nm = nmap.PortScanner()
        a = nm.scan(hosts=str(cidr), arguments='-sS -sV -T4 -O') #nmap -sS -T4 -A -sC -oA scanname
        for ip, ip_values in a['scan'].items():
            if not self.ip_state_up(ip_values):
                continue

            ip_data = {'ip': ip}
            if 'addresses' in ip_values and ip_values['addresses']:
                ip_data.update(ip_values['addresses'])
            if 'tcp' in ip_values and ip_values['tcp']:
                ip_data.update({'ports': list(ip_values['tcp'].keys())})
            if 'vendor' in ip_values and ip_values['vendor'] and type(ip_values['vendor'] is dict):
                ip_data.update({'vendor': list(ip_values['vendor'].values())[0]})
            if 'osmatch' in ip_values and ip_values['osmatch'] and type(ip_values['osmatch']) is list:
                osmatch_name_list = []
                for osmatch in ip_values['osmatch']:
                    if 'name' in osmatch and osmatch['name']:
                        osmatch_name_list.append(osmatch['name'])
                ip_data.update({'osmatch': osmatch_name_list})

            self.devices_list.append(ip_data)
        return self.devices_list

    def ip_state_up(self, ip_values):
        return 'status' in ip_values \
               and ip_values['status'] \
               and 'state' in ip_values['status'] \
               and ip_values['status']['state'] \
               and str(ip_values['status']['state']) == 'up'
