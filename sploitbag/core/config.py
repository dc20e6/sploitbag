import json

from sploitbag import config_file_path


class Config:
    class __impl:
        def __init__(self):
            self.config = json.load(open(config_file_path(), 'r'))

        def configs(self):
            return self.config

    __instance = None

    def __init__(self):
        if Config.__instance is None:
            Config.__instance = Config.__impl()
        self.__dict__['_Singleton__instance'] = Config.__instance

    def __getattr__(self, attr):
        return getattr(self.__instance, attr)

    def __setattr__(self, attr, value):
        return setattr(self.__instance, attr, value)
