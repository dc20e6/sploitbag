import subprocess
import logging


class CustomClient(object):
    def __init__(self, protocol, address, port):
        self.protocol = protocol
        self.address = address
        self.port = port

    def execute(self, *commands):
        command_string = ''
        for command in commands:
            command_string += '{}; '.format(command)
        logging.info('{}: {}: {}'.format(self.protocol, self.address, command_string))
        subprocess.call(command_string, shell=True)
        return True
