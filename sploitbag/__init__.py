import pkg_resources

config_file_path = lambda: pkg_resources.resource_filename(__name__, 'config.json')
