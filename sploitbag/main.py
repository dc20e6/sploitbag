import sys
import netaddr
import logging
import itertools

from collections import Counter
# from pymsfrpc import msfrpc

from sploitbag.core.config import Config

from sploitbag.core.utils import detect_cidrs

from sploitbag.core.nmap import NetworkMapper
from sploitbag.core.exploit import FindExploit


class SploitBagInterpreter(object):

    def __init__(self, args):
        self.cidrs = detect_cidrs() if args.CIDR == 'autoheck' else [args.CIDR]
        self.a = args.a
        self.c = args.c
        self.exploited = 0
        self.config = Config().configs()

        self.msfrpcd = '{} {} {}'.format(self.config['METASPLOIT']['server_ip'],
                                         self.config['METASPLOIT']['username'],
                                         self.config['METASPLOIT']['password'])
        print(self.msfrpcd)  # TODO: инициилизация msf_rpcd и переделать нах

    def start(self):
        for cidr in self.cidrs:
            try:
                cidr = netaddr.IPNetwork(cidr)
            except (netaddr.core.AddrFormatError, ValueError):
                logging.critical('Network ({}) is not in CIDR presentation format'.format(cidr))
                continue

            if self.a == 'mapping':
                self.mapping(cidr, True)
            elif self.a == 'exploiting':
                self.exploiting(cidr)

    def stat(self, status):
        if status:
            self.exploited += 1

    def mapping(self, cidr, verbose):
        network = NetworkMapper().map_network(cidr)
        if verbose:
            device_count = len(network)
            openned_ports_count = 0
            openned_ports_list = []
            vendors = []
            for device in network:
                if 'ports' in device and device['ports']:
                    openned_ports_count += len(device['ports'])
                    openned_ports_list.append(device['ports'])
                if 'vendor' in device and device['vendor']:
                    vendors.append(device['vendor'])
            all_ports = []
            for port_list in openned_ports_list:
                all_ports += itertools.chain(port_list)
            ports = Counter(all_ports)
            logging.info('\n'
                         'Total devices count: {} \n'
                         'Total count of openned ports discovered: {} \n'
                         'Ports: {} \n'
                         'All vendors: {}'.format(device_count,
                                                  openned_ports_count,
                                                  ports,
                                                  ', '.join(set(vendors))))
        return network

    def exploiting(self, cidr):
        while True:
            network = self.mapping(cidr, False)
            for device in network:
                logging.debug(device)
                self.stat(FindExploit(device).find_exploit())
            logging.info('Exploited ~{} devices'.format(self.exploited))
            if not self.c:
                logging.info('See you soon :)')
                sys.exit()
